const express = require('express');
const controller = require('../controllers/users');
const router = express.Router();

router.post('/', controller.create);

router.get('/', controller.list);

router.get('/:id', controller.index);

router.put('/:id', controller.edit);

router.patch('/:id', controller.replace);

router.delete('/:id', controller.remove);

module.exports = router;
