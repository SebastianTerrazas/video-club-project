const express = require('express');

//model User = {email, name, lastName, password}

function create(req, res, next) {
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;

    let user = new Object({
        email: email,
        name: name,
        lastName: lastName,
        password: password
    });

    console.log(user);

    res.json(user);
}

function list(req, res, next) {
    res.send('Mostrar todos los usuarios del sistema.');
}

function index(req, res, next) {
    const id = req.params.id;
    res.send(`Mostrar el usuario del sistema con el id = ${id}`);
}

function edit(req, res, next) {
    const id = req.params.id;
    res.send(`Modificar el usuario del sistema con el id = ${id}`);
}

function replace(req, res, next) {
    const id = req.params.id;
    res.send(`Reemplazar el usuario del sistema con el id = ${id}`);
}

function remove(req, res, next) {
    const id = req.params.id;
    res.send(`Elimina el usuario del sistema con el id = ${id}`);
}

module.exports = {
    create, list, index, edit, replace, remove
}